import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationService } from './services/notification.service';
import { ToastrModule } from 'ngx-toastr';
import { BackendService } from './services/backend.service';
import { MatTableModule } from '@angular/material/table'  
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
@NgModule({
  declarations: [],
  providers: [NotificationService, BackendService],
  imports: [
    CommonModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      positionClass: 'toast-top-right',
      closeButton: true,
      progressBar: true,
      
    }),
    MatTableModule,
    MatInputModule,
  ],
  exports:[ MatTableModule, MatFormFieldModule ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core Module already initiated!');
    }
  }
}
