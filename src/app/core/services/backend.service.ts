import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { userDTO } from 'src/app/models/users/user-dto';
import { postDTO } from 'src/app/models/posts/post-dto';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NotificationService } from './notification.service';
@Injectable({
    providedIn: 'root',
  })
  export class BackendService {
    private readonly url = 'https://jsonplaceholder.typicode.com';
    constructor(
      private http: HttpClient,
      private readonly router: Router,
      private readonly notification: NotificationService,
    ) {

    }

    public getUsers(): Observable<userDTO[]> {
    let users = this.http.get<userDTO[]>(`${this.url}/users`)
      return users;
  }
    public getUserPosts(userId: string): Observable<any[]> {


    let posts = this.http.get<any[]>(`${this.url}/posts?userId=${userId}`)

      return posts;
      }
}

