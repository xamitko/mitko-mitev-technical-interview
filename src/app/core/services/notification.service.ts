import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  constructor(private readonly toastr: ToastrService) {}
  success(msg: string): void {
    this.toastr.success(msg);
  }
  error(error: string): void {
    this.toastr.error(error);
  }
  info(info: string): void {
    this.toastr.info(info);
  }
}
