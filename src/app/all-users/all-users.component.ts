import {
    animate,
    state,
    style,
    transition,
    trigger,
} from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { BackendService } from '../core/services/backend.service';
import { userDTO } from '../models/users/user-dto';
import {
    attachUserPosts,
    editUser,
    fetchUsers,
} from '../store/actions/users.actions';
import { usersReducer } from '../store/reducers/users.reducer';
import { IState } from '../store/state.interface';
import { UserEditDialogComponent } from '../user-edit-dialog/user-edit-dialog.component';

@Component({
    selector: 'app-all-users',
    templateUrl: './all-users.component.html',
    styleUrls: ['./all-users.component.sass'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition(
                'expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
            ),
        ]),
    ],
})
export class AllUsersComponent implements OnInit {
    public users: userDTO[] = [];
    public editForm: FormGroup;
    public expandedElement: userDTO | null;
    constructor(
        public dialog: MatDialog,
        private readonly toastr: ToastrService,
        private readonly store: Store<IState>
    ) {}

    startEdit(row: any) {
        this.dialog
            .open(UserEditDialogComponent, {
                data: row,
            })
            .afterClosed()
            .subscribe((res) => {
                if (res.event) {
                    this.toastr.success('Editing canceled');
                } else {
                    this.toastr.success('User edited successfully');
                    this.store.dispatch(editUser({ user: res.data }));
                }
            });
    }

    getUserPosts(row: any) {
        if (!row.Posts) {
            this.toastr.success(`User's posts downloaded`);
        }
        this.store.dispatch(attachUserPosts({user:row, id: row.id }));
    }
    ngOnInit(): void {
        this.store.dispatch(fetchUsers());
        this.users = this.store.select('users') as any;
        // nov action koito da vzema ot stora chrez selectori
        // this.backendService.getUsers().subscribe((data) => (this.users = data));
    }
    readonly displayedColumns: string[] = [
        'adress.city',
        'adress.street',
        'adress.suite',
        'company',
        'email',
        'id',
        'name',
        'phone',
        'username',
        'website',
        'action',
    ];
}
