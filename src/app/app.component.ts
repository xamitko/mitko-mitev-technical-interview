import { Component } from '@angular/core';
import { BackendService } from 'src/app/core/services/backend.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  title = 'my-app';

  }

