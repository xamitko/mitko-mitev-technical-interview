import { Component, Inject, OnInit } from '@angular/core';
import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { userDTO } from '../models/users/user-dto';

@Component({
    selector: 'app-user-edit-dialog',
    templateUrl: './user-edit-dialog.component.html',
    styleUrls: ['./user-edit-dialog.component.sass'],
})
export class UserEditDialogComponent implements OnInit {
    public editForm: FormGroup;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: userDTO,
        private readonly fb: FormBuilder,
        public dialogRef: MatDialogRef<UserEditDialogComponent>
    ) {}

    ngOnInit(): void {
        this.editForm = this.fb.group({
            address: this.fb.group({
                city: new FormControl(this.data.address.city, []),
                street: new FormControl(this.data.address.street, []),
                suite: new FormControl(this.data.address.suite, []),
            }),
            name: new FormControl(this.data.name, []),
            phone: new FormControl(this.data.phone, []),
            website: new FormControl(this.data.website, []),
            id: new FormControl(this.data.id, []),
        });
    }
    updateUser() {
        this.dialogRef.close({ data: this.editForm.value });
    }

    cancelUpdate() {
        this.dialogRef.close({ event: 'Cancel' });
    }
}
