import { postDTO } from '../posts/post-dto';
import { adressDTO } from './adress-dto ';
import { companyDTO } from './company-dto';

export class userDTO {
    address:adressDTO;
    company:companyDTO;
    email:string;
   id:string;
 name:string;
 phone:string;
 username:string;
 website:string;
 isExpanded: boolean
 Posts?:postDTO[];
}
