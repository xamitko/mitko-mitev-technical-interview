import { adressDTO } from './adress-dto ';

export class userEditDTO {
    adress:adressDTO;
    name:string;
    website:string;
    phone:string;
    id:string;

}
