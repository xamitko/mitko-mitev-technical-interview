import { createSelector } from '@ngrx/store';
import { userDTO } from 'src/app/models/users/user-dto';
import { IState } from '../state.interface';

export const selectUsers = createSelector(
    (state: IState) => state.users,
    (users: Array<userDTO>) => users
  );
