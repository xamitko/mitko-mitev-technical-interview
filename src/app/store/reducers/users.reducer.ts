import { createReducer, on } from '@ngrx/store';
import { userDTO } from 'src/app/models/users/user-dto';
import {
    attachUserPosts,
    editUser,
    fetchUsers,
} from '../actions/users.actions';

export const initialState: userDTO[] = [];

const _usersReducer = createReducer(
    initialState,
    on(editUser, (state: userDTO[], { user }: { user: userDTO }) => {
        console.log('in edituser');
        console.log(state);

        return state.map((el) => {
            if (el.id === user.id) {
                return {
                    ...el,
                    ...user,
                };
            }
            return el;
        });
    }),
    on(fetchUsers, (state, userResponse) => {
        // console.log((userResponse as any).users);
        return (userResponse as any).users;
    }),
    on(attachUserPosts, (state: any, userPosts: any) => {
        if (userPosts.posts) {
            console.log(state);
            console.log(userPosts);
            return state.map((el: any) => {
                if (el.id === userPosts.posts[0].userId) {
                    el.posts = userPosts.posts;
                    console.log('2nd inf');

                    return el;
                }
                return el;
            });
        }

        return state;
    })
);

export function usersReducer(state: userDTO[], action: any) {
    return _usersReducer(state, action);
}
