import { createAction, props } from '@ngrx/store';
import { userDTO } from 'src/app/models/users/user-dto';

export const USER_EDIT_ACTION = '[Edit users] Edit';
export const USER_ATTACHPOSTS_ACTION = '[Edit users] AttachPost'
export const editUser = createAction(
    USER_EDIT_ACTION,
    props<{ user: userDTO }>()
);
export const attachUserPosts = createAction(
    USER_ATTACHPOSTS_ACTION,
    props<{ user: userDTO, id: string }>()
);

export const USER_FETCH_ACTION = '[Edit users] Fetch';
export const fetchUsers = createAction(USER_FETCH_ACTION);
