import { userDTO } from '../models/users/user-dto';

export interface IState {
    users: userDTO[],
}
