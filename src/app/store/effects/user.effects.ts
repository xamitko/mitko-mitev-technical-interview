import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { catchError, map, mergeMap, switchMap, take } from 'rxjs/operators';
import { BackendService } from 'src/app/core/services/backend.service';
import {
    USER_ATTACHPOSTS_ACTION,
    USER_FETCH_ACTION,
} from '../actions/users.actions';

@Injectable()
export class UserEffects {
    fetchUsers$ = createEffect(() =>
        this.actions$.pipe(
            ofType(USER_FETCH_ACTION),
            mergeMap(() =>
                this.backendService.getUsers().pipe(
                    map((users) => ({ type: USER_FETCH_ACTION, users })),
                    catchError(() => EMPTY)
                )
            ),
            take(1),
        )
    );
    fetchUserPosts$ = createEffect(() =>
        this.actions$.pipe(
            ofType(USER_ATTACHPOSTS_ACTION),
            switchMap((action: any) =>
                this.backendService.getUserPosts(action.id).pipe(
                    map((posts) => ({ type: USER_ATTACHPOSTS_ACTION, posts })),
                    catchError(() => EMPTY),
                ),
            ),
            take(1),
        )
    );
    constructor(
        private actions$: Actions,
        private readonly backendService: BackendService
    ) {}
}
