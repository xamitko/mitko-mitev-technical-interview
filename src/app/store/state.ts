import { combineReducers } from '@ngrx/store';
import { usersReducer } from './reducers/users.reducer';
import { IState } from './state.interface';

export const state = combineReducers<IState>({
    users: usersReducer,
} as any); // https://github.com/reduxjs/redux/issues/2709#issuecomment-357328709
